import javax.swing.*;
import java.util.ArrayList;
import java.util.Collections;

public class Main {

    public static void main(String[] args) {
        String input = "";
        int valid = -1;
         int valid2 = -1;
        int i=0;
        boolean[] validation = new boolean[9];
        boolean validTwo = false;
        ArrayList<Character> in = new ArrayList<Character>();
        int g = 0;
        ArrayList<Node> openList = new ArrayList<>();
        ArrayList<Node> closedList = new ArrayList<>();
        int idCount =1;
        int index = 0;

// ***********Input Validation Section*********************//
        while (valid == -1) {
            input = JOptionPane.showInputDialog(null, "please enter start state sequence");
            valid = validInput(input);
            input = input.replace(" ", "");

            for (i = 0; i < input.length(); i++) {
                in.add(input.charAt(i));
            }

            for (int j = 0; j < in.size(); j++) {
                for (int k = 0; k < in.size(); k++) {
                    if (in.get(j).equals(in.get(k)) && j != k) {
                        JOptionPane.showMessageDialog(null, "Repeated digit, try again");
                        j = in.size();
                        k = in.size();
                        in.clear();
                        valid = -1;
                    }
                }
            }
        }
        int[] board = charToIntArray(in);
        in.clear();
        input="";
        while (valid2 == -1) {
            input = JOptionPane.showInputDialog(null, "please enter goal state sequence");
            valid2 = validInput(input);
            input = input.replace(" ", "");

            for (i = 0; i < input.length(); i++) {
                in.add(input.charAt(i));
            }

            for (int j = 0; j < in.size(); j++) {
                for (int k = 0; k < in.size(); k++) {
                    if (in.get(j).equals(in.get(k)) && j != k) {
                        JOptionPane.showMessageDialog(null, "Repeated digit, try again");
                        j = in.size();
                        k = in.size();
                        in.clear();
                        valid2 = -1;
                    }
                }
            }
        }
        int[] goalState = charToIntArray(in);

        //*****End of Validation, Beginning of A star search***********//

           int rows = 3;
           int cols = 3;

           String one="";
            String two="";
            String three="";

           int[][] twoDBoard = new int[cols][rows];

           for(i =0; i<cols; i++){
               for(int j=0; j<rows; j++){
                   twoDBoard[j][i] = board[i+rows*j];
               }
           }

// find the zero WE SHOULD MAKE THIS INTO A SEPARATE METHOD//
           int rowPosition=-1;
           int colPosition = -1;
           for( i =0; i<cols; i++){
               for(int j=0; j<rows; j++){
                   if(twoDBoard[i][j] == 0){
                       rowPosition = j;
                       colPosition = i;
                   }
               }
           }

           String theBoard = boardtoString(rows,cols,twoDBoard);
           boolean left=false, right=false, up=false, down=false;
           int zeroIndex = theBoard.indexOf("0");
             System.out.println(theBoard);
           if(zeroIndex==0)
           {left=false;
           }
           else if(Character.isSpaceChar(theBoard.charAt(zeroIndex-1))){
               left=true;
               System.out.println(twoDBoard[colPosition][rowPosition-1]+" to the West");
           }

           if (theBoard.substring(zeroIndex+1).equalsIgnoreCase(".")){
               right=false;
           }
           else if(Character.isSpaceChar(theBoard.charAt(zeroIndex+1))){
               right=true;
               System.out.println(twoDBoard[colPosition][rowPosition+1]+" to the East");
           }

           if(colPosition>0){
               up=true;
               System.out.println(twoDBoard[colPosition-1][rowPosition]+" to the North");
           }

           if(colPosition<cols-1){
               down=true;
               System.out.println(twoDBoard[colPosition+1][rowPosition]+" to the South");
           }


    }

       public static String boardtoString(int rows, int cols, int[][] twoDBoard){
        String theboard="";
        int i=0;
        int j =0;
                for(i=0; i<cols; i++){
                    for(j=0; j<rows; j++){
                   theboard = theboard + twoDBoard[i][j] + " ";
               }
               theboard = theboard.substring(0,theboard.lastIndexOf(" "))+"\n";
           }
           theboard = theboard.substring(0,theboard.lastIndexOf("\n"))+".";
        return theboard;
       }

       //used in validation to convert input//
    public static int[] charToIntArray(ArrayList<Character> in) {

        int[] board = new int[in.size()];
        for (int i = 0; i < board.length; i++) {
            board[i] = Integer.parseInt(in.get(i).toString());
        }
        return board;
    }
    //Regex setup method//
    public static int validInput(String input) {
        if (input.matches("[0-8]\\s[0-8]\\s[0-8]\\s[0-8]\\s[0-8]\\s[0-8]\\s[0-8]\\s[0-8]\\s[0-8]{1}")) {
            return 1;
        } else {
            JOptionPane.showMessageDialog(null,"invalid input, please input a valid sequence");
            return -1;
        }
    }
}


/*
        put the current node in the closed list and look at all of its neighbors
        for (each neighbor of the current node):
        if (neighbor has lower g value than current and is in the closed list) :
        replace the neighbor with the new, lower, g value
        current node is now the neighbor's parent
        else if (current g value is lower and this neighbor is in the open list ) :
        replace the neighbor with the new, lower, g value
        change the neighbor's parent to our current node

        else if this neighbor is not in both lists:
        add it to the open list and set its g
*/



// Code Cemetery //

       /* int heuristic =0;

        int[] winningBoard = new int[]{1,2,3,4,5,6,7,8,0};
        int[] firstState = new int[]{8,2,3,4,1,5,6,7,0};
        int[] yCoords = new int[]{0,0,0,1,1,1,2,2,2};
        int[] xCoords = new int[]{0,1,2,0,1,2,0,1,2};

        int value;
        int dx=0;
        int dy =0;
        int dxdy=0;

        for(i=0; i<9; i++){
            value = firstState[i];
            dx = Math.abs(xCoords[i] - xCoords[value-1]);
            dy = Math.abs(yCoords[i] - yCoords[value-1]);
            dxdy = dx+dy;
            heuristic+=dxdy;
            System.out.print(heuristic + ",");
        }
        System.out.println(heuristic);
*/
/* for (i=0; i<9; i++) {
            System.out.println(" goal " +goalState[i]);
            System.out.println(" passed " +passedState[i]);
        }*/



           /*
           switch(index){
               case 0 {

               }
               case 1{}
               case 2{}
               case 3{}
               case 4{}
               case 5{}
               case 6{}
               case 7{}
               case 8{}

           }

       }*/



        /* System.out.println("1 2 3\n0 5 6\n7 8 4");

        while(!goal.isReached()){
                    Node current = openList.get(0);

       if(current.equals(goal)){
           System.out.println("done");
       }
       else {
           index = current.position();
           closedList.add(current);
*/