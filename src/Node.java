public class Node implements Comparable{
    int  g;
    int h;
    Node parent;
    int id;
    int parentId;
    boolean reached;
    int f;
    int position;
    int[] state = new int[9];

    public Node(int g, int h, int id, int parentId, int position, int[] state) {

        this.g = g;
        this.h = h;
        this.id = id;
        this.parentId= parentId;
        this.f= g+h;
        this.position = position;
        this.state = state;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public int[] getState() {
        return state;
    }

    public void setState(int[] state) {
        this.state = state;
    }

    public boolean isReached() {
        return reached;
    }

    public int getF() {
        return f;
    }

    public void setF(int f) {
        this.f = f;
    }

    public Node getParent() {
        return parent;
    }

    public void setParent(Node parent) {
        this.parent = parent;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getParentId() {
        return parentId;
    }

    public void setParentId(int parentId) {
        this.parentId = parentId;
    }

    public void setReached(boolean reached) {
        this.reached = reached;
    }

    public int getG() {
        return g;
    }

    public void setG(int g) {
        this.g = g;
    }

    public int getH() {
        return h;
    }

    public void setH(int h) {
        this.h = h;
    }

    @Override
    public int compareTo(Object o) {
        int compareF = ((Node)o).getF();
        return this.getG()-compareF;
    }

    public int position(){
        for(int i=0; i<9; i++){
            if (state[i]== 0){
                return i;
            }
        }
        return -1;
    }
}